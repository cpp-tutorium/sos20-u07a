#pragma once



class cWaggon {
private:
	// Klasse nur minimal definieren
	double laenge;
	double gewicht;
public:
	cWaggon (double len_in = 14.8, double gew_in = 117.5)
		: laenge (len_in), gewicht(gew_in) {}

	void ausgabe();
};

