#include "cZug.h"
#include <iostream>
#include <cstdlib>


cZug::cZug(cLok lok_in, std::string bezeichnung_in, int anzahlWaggons_in)
	: lokomotive(lok_in), bezeichnung(bezeichnung_in), anzahlWaggons(anzahlWaggons_in)
{
	waggons = new cWaggon[anzahlWaggons];
	wagenWerte();
}

cZug::cZug(const cZug& cZug_in)
{
	bezeichnung = cZug_in.bezeichnung;
	lokomotive = cZug_in.lokomotive;
	anzahlWaggons = cZug_in.anzahlWaggons;

	waggons = new cWaggon[anzahlWaggons];
	wagenWerte();
}

cZug::~cZug()
{
	std::cout << "cZug destruktor: " << bezeichnung << std::endl;
	delete[] waggons;
}

void cZug::ausgabe()
{
	std::cout << "Zug Bezeichnung: " << bezeichnung << ", Wagen anzahl: " << anzahlWaggons << std::endl;
	lokomotive.ausgabe();
	for (int i = 0; i < anzahlWaggons; ++i)
	{
		waggons[i].ausgabe();
	}
	std::cout << std::endl;
}

void cZug::wagenWerte()
{
	for (int i = 0; i < anzahlWaggons; ++i)
	{
		// laenge zwischen 8.50 und 35.80 differenze = 27.3
		const double laenge = ((std::rand() % 273) / 10.0) + 8.50;
		// Gewicht zwischen 12.5 und 120.0 differenze = 107.5
		const double gewicht = ((std::rand() % 1075) / 10.0) + 12.5;

		waggons[i] = cWaggon(laenge, gewicht);
	}
}
