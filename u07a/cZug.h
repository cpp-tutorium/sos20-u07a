#pragma once
#include <string>
#include "cLok.h"
#include "cWaggon.h"


class cZug
{
private:
	std::string bezeichnung;
	cLok lokomotive;
	int anzahlWaggons;
	cWaggon* waggons;

public:
	cZug(cLok lok_in, std::string bezeichnung_in, int anzahlWaggons_in);
	cZug(const cZug &cZug_in);
	~cZug();

	void ausgabe();

private:
	void wagenWerte();
};

