#include <iostream>
#include <ctime>
#include <cstdlib>
#include "cLok.h"
#include "cZug.h"


int main()
{
	// Setzen des random seeds EINMAL am anfang
	std::srand(std::time(0));

	cLok e40;
	cZug gueterZug(e40, "G23421", 45); 
	cZug gueterZug2(gueterZug); // Konstruktion mit Kopierkonstruktor
	cLok e85(21000);
	cZug gueterSchnellZug(e85, "GS4444", 12);

	gueterZug.ausgabe();		// ersten Zug ausgeben
	gueterZug2.ausgabe();		// kopierten Zug ausgeben
	gueterSchnellZug.ausgabe();	// dritten Zug ausgeben

	std::cout << std::endl;

	return 0;
}
